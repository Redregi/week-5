#Josh "Slangdongler P.H.D." Churchill
#09/29/2015
#Bonus Project
import random
computerGame = True
while computerGame:
    ranNum = random.randint(1, 4)
    gameQ = input("Choose rock, paper, or scissors ")
    if(ranNum == 1):
        print("The computer chose rock")
        if(gameQ == "rock"):
            print("Tie.")
        if(gameQ == "paper"):
            print("You win!")
        if(gameQ == "scissors"):
            print("You lose.")

    if(ranNum == 2):
        print("The computer chose paper")
        if(gameQ == "rock"):
            print("You lose.")
        if(gameQ == "paper"):
            print("Tie.")
        if(gameQ == "scissors"):
            print("You win!")

    if(ranNum == 3):
        print("The computer chose scissors")
        if(gameQ == "rock"):
            print("You win!")
        if(gameQ == "paper"):
            print("You lose.")
        if(gameQ == "scissors"):
            print("Tie.")

    gameContinue = input("Do you wish to continue? Y/N ")
    if(gameContinue == "N" or gameContinue == "n"):
        print("Have a nice day.")
        computerGame = False



